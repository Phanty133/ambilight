from sklearn.cluster import KMeans
import numpy as np
import mss
from screeninfo import get_monitors

import cv2
from PIL import Image
import time
import pygame
from pygame.locals import *
import sys

display = pygame.display.set_mode((1088, 644))

monitorInfo = get_monitors()
monitorRes = {"x": monitorInfo[1].width, "y": monitorInfo[1].height}

sectorCount = 50 #How many sub divisions there are in total
sectorHeight = 100 #How many pixels high should the divisions be
sideDivisionHeight = monitorRes["y"] - 2 * sectorHeight
sectorWidth = int((monitorRes["x"] * 2 + 2 * sideDivisionHeight) / sectorCount)

divisionArea = {
	"top": {
		"top": 0,
		"left": 0,
		"width": monitorRes["x"],
		"height": sectorHeight
	},
	"left": {
		"top": sectorHeight,
		"left": 0,
		"width": sectorHeight,
		"height": sideDivisionHeight
	},
	"right": {
		"top": sectorHeight,
		"left": monitorRes["y"] - sectorHeight,
		"width": sectorHeight,
		"height": sideDivisionHeight
	},
	"bottom": {
		"top": monitorRes["y"] - sectorHeight,
		"left": 0,
		"width": monitorRes["x"],
		"height": sectorHeight
	}
}

vertSectorCount = round(sideDivisionHeight / sectorWidth)
horizSectorCount = round(monitorRes["x"] / sectorWidth)

def getImagePixels(image):
	rgb_tuples = zip(
		image.raw[2::4], image.raw[1::4], image.raw[0::4]
	)  # type: Iterator[Pixel]

	return np.asarray(tuple(zip(*[iter(rgb_tuples)] * image.width)))

def getImagePixelsOptimized(screenshot):
	rgbArr = np.vstack([
		np.asarray(screenshot.raw[2::4], dtype=np.uint8),
		np.asarray(screenshot.raw[1::4], dtype=np.uint8),
		np.asarray(screenshot.raw[0::4], dtype=np.uint8)
	])

	return np.reshape(np.rot90(rgbArr), (screenshot.height, screenshot.width, 3))

def getAreaPixels(pixelArray, x, y, width, height):
	return pixelArray[y:y + height, x:x + width, :]

avgFPSArr = []
timeLimit = 120

with mss.mss() as sct:
	timeStart = time.time()
	while True:
		start = time.time()
		processedAreas = {}

		for area, captureArea in divisionArea.items():
			screenshot = sct.grab(captureArea)
			screenPixels = getImagePixelsOptimized(screenshot)

			curArea = None

			if area == "top" or area == "bottom":
				for curSector in range(horizSectorCount):
					sectorCapture = getAreaPixels(screenPixels, curSector * sectorWidth, 0, sectorWidth, sectorHeight)
					flatArea = np.reshape(sectorCapture, (int(sectorCapture.size / 3), 3))
					dominantColor = KMeans(n_clusters=1, n_init=1, random_state=0, max_iter=10, precompute_distances=False, algorithm="full").fit(flatArea).cluster_centers_

					colorImg = np.full((64,64,3), dominantColor)
					curArea = colorImg if curArea is None else np.concatenate((curArea, colorImg), axis=1)
			else:
				for curSector in range(vertSectorCount):
					sectorCapture = getAreaPixels(screenPixels, 0, curSector * sectorWidth, sectorHeight, sectorWidth)
					flatArea = np.reshape(sectorCapture, (int(sectorCapture.size / 3), 3))
					dominantColor = KMeans(n_clusters=1, n_init=1, random_state=0, max_iter=10, precompute_distances=False, algorithm="full").fit(flatArea).cluster_centers_

					colorImg = np.full((64,64,3), dominantColor)
					curArea = colorImg if curArea is None else np.concatenate((curArea, colorImg), axis=0)

			#imgOutput = curArea.astype("float32") / 255

			processedAreas[area] = curArea#imgOutput
		
		
		avgFPSArr.append(time.time() - start) 

		if time.time() - timeStart > timeLimit:
			break
		
		
		for event in pygame.event.get():
			if event.type in (QUIT, KEYDOWN):
				print("Frame time: %f ms" %(sum(avgFPSArr) / len(avgFPSArr)))
				print("Average FPS: ", 1000 / sum(avgFPSArr) / len(avgFPSArr)) 

				break

		output = np.hstack((processedAreas["left"], np.zeros((512, 960, 3)), processedAreas["right"]))
		output = np.vstack((processedAreas["top"], output, processedAreas["bottom"]))

		#cv2.imshow("test", output[...,::-1].copy())
		#cv2.waitKey(0)

		output = np.rot90(output)

		surface = pygame.surfarray.make_surface(output[...,::-1].copy())

		display.blit(surface, (0, 0))
		pygame.display.update()
		

	print("Frame time: %f s" %(sum(avgFPSArr) / len(avgFPSArr)))
	print("Average FPS: ", 1 / (sum(avgFPSArr) / len(avgFPSArr)))